[TOC]

## WHATS THIS

This is compass that shows not only direction to north, but also direction
to any location you choose.

You can get location(s) by SMS, for instance, from your friend. Or send your
location to a friend.

## WHY ITS GOOD

* Suitable for use on motorbike or in a car
* You dont need Internet connection to exchange locations - any mobile network
  will do
* Small size - you can download it on any connection

## BASIC USAGE

* [Install from Google Play][]

[Install from Google Play]: https://play.google.com/store/apps/details?id=com.sgr_b2.compass

### SMS locations import

If you have any new SMS with location inside, app will open import dialog
automagically.

Otherwise, open menu -> Bookmarks, open menu again -> Import bookmarks

### Sending your current location

If you have a GPS fix (you've got your coords from satellites), open menu ->
Send location.

## MANAGING BOOKMARKS

Open menu -> Bookmarks

### Adding arbitrary bookmark

Open menu -> Add bookmark, then enter bookmark title and coordinates
(latitude, longitude). If you have a GPS fix (see above), coordiantes will be
automatically filled in.

### Editing and deleting bookmarks

Long touch bookmark, choose "Edit" or "Delete" from appeared menu.

### Sending bookmarked locations

Long touch bookmark, choose "Send" from appeared menu.

Your default SMSing application will appear, there you can choose recipient
and look into SMS text.

## IMPORTING BOOKMARKS

Open menu -> Bookmarks -> open menu -> Import bookmarks.

Select any unchecked location found in your SMS messages. There can be only
one location with the same name, therefore if you import location that
collides on name with existing location, old one is being replaced.

Worry not, you can always import old location back if you still have it
in your SMS messages.

## SETTINGS

### Units

This setting mainly control how to display distance: in meters or feet.

### Theme

You can choose any you like the most.

### Debug info

Normally you don't need it, this one is for me personally. If you are
curious, it will show you:

* Number of GPS satellites (and accuracy in meters) in top-left corner
* Current speed in bottom-left corner (meters per second)
* Direction value in bottom-right corner (degrees)
* Current location and target location under target title

## KNOWN ISSUES

* TBD

## DEVELOPERS CORNER

### HRT ♡ protocol

HRT == Human Readable Text. This is format used to exchange locations by SMS.
Syntax is very simple:

* location         = <title> [latitude,longitude]
* title            = any string, might include spaces
* latitude         = double in text representation, any precision
* longitude        = same as latitude

Title is optional. Any text after longitude should be ignored.

Example:

    Kiev 50.462077,30.526421

Main goals of HRT:

* You can enter any location manually
* You don't have to input valid XML to do so
* You can see what you're sending
* You can read what you've received
* You can add note (title) to a location

## ROADMAP

Currently this is only prototype of application in Java, really.

1. Distress call
2. Extracting main part into portable C-code
3. iOS support
