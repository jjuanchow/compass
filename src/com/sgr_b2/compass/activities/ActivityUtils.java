package com.sgr_b2.compass.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.io.HRTProtocol;


public class ActivityUtils {
	public static void sendPOI(Context context, POI poi) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.addCategory(Intent.CATEGORY_DEFAULT);
		i.setType("vnd.android-dir/mms-sms");

		i.putExtra("sms_body", HRTProtocol.marshall(poi));

		context.startActivity(i);
	}

	/**
	 * start system SMS sending activity
	 */
	public static void sendLocation(Context context, Location location) {
		sendPOI(context,
			new POI(location.getLatitude(), location.getLongitude(), null));
	}
}
