package com.sgr_b2.compass.activities;

import java.text.ParseException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.db.POIProvider;
import com.sgr_b2.compass.navigation.Utils;
import com.sgr_b2.compass.ui.UnitsFormatting;


public class AddEditActivity extends Activity implements OnClickListener {

	public static final int MODE_ADD = 1;
	public static final int MODE_EDIT = 2;

	private POIProvider provider = null;

	private POI poi = null;
	private int mode = -1;
	private Location current_location = null;

	public class Extras {
		public static final String MODE = "mode";
		public static final String POI = "poi";
		public static final String LOCATION = "current_location";
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.provider = new POIProvider(this);

		Intent intent = getIntent();
		this.mode = intent.getIntExtra(Extras.MODE, -1);
		this.poi = (POI)intent.getSerializableExtra(Extras.POI);
		this.current_location = (Location)intent.getParcelableExtra(Extras.LOCATION);

		assert(this.mode == MODE_ADD || this.mode == MODE_EDIT);
		assert(this.mode == MODE_ADD || this.poi != null);

		if (mode == MODE_ADD) {
			this.poi = new POI(0, 0, "");
		}

		assert(this.poi != null);

		setContentView(R.layout.activity_addedit);
		setTitle(this.mode == MODE_ADD
			? getResources().getString(R.string.title_add_bookmark)
			: getResources().getString(R.string.title_edit_bookmark));

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		Button ok = (Button)findViewById(R.id.ok_button);
		ok.setOnClickListener(this);

		EditText poi_title = (EditText)findViewById(R.id.title_text);
		EditText poi_lat = (EditText)findViewById(R.id.lat_text);
		EditText poi_lon = (EditText)findViewById(R.id.lon_text);

		if (mode == MODE_EDIT) {
			poi_title.setText(String.valueOf(this.poi.title));
			poi_lat.setText(UnitsFormatting.formatCoord(this.poi.lat));
			poi_lon.setText(UnitsFormatting.formatCoord(this.poi.lon));
		}
		else if (this.current_location != null) {
			poi_lat.setText(UnitsFormatting.formatCoord(this.current_location.getLatitude()));
			poi_lon.setText(UnitsFormatting.formatCoord(this.current_location.getLongitude()));
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (this.provider != null) {
			this.provider.dispose();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (this.current_location != null) {
			Toast.makeText(this,
				getResources().getString(R.string.toast_location_preloaded),
				Toast.LENGTH_SHORT)
			.show();
		}
	}

	private void maybeReplacePOI(final String title, final double lat, final double lon) {
		POI poi = this.provider.hasPOI(title);

		// new POI w/o conflicts
		if (poi == null && this.mode == MODE_ADD) {
			this.provider.addPOI(new POI(lat, lon, title));
			finish();

			return;
		}

		// same POI, no real conflict
		if (this.mode == MODE_EDIT && this.poi.id == poi.id) {
			this.provider.updatePOI(new POI(lat, lon, title));
			finish();

			return;
		}

		// real conflict
		final POIProvider provider = this.provider;
		final POI this_poi = this.poi;
		final int this_mode = this.mode;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setMessage(R.string.message_bookmark_exists);
		builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// delete edited POI
				if (this_mode == MODE_EDIT) {
					provider.deletePOI(this_poi.title);
				}

				// what if power is interrupted at this point?
				// this is terrifying

				// then replace conflicted
				provider.updatePOI(new POI(lat, lon, title));
				finish();
			}
		});
		builder.setNegativeButton(android.R.string.no, null);
		builder.show();
	}

	private void showOKAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setMessage(message);
		builder.setPositiveButton(android.R.string.ok, null);

		builder.show();
	}

	@Override
	public void onClick(View v) {
		String poi_title = ((EditText)findViewById(R.id.title_text)).getText().toString();
		String poi_lat = ((EditText)findViewById(R.id.lat_text)).getText().toString();
		String poi_lon = ((EditText)findViewById(R.id.lon_text)).getText().toString();

		if (poi_title.length() < 1) {
			showOKAlert(getResources().getString(R.string.message_invalid_title));
			return;
		}

		if (poi_lat.length() < 1 || poi_lon.length() < 1) {
			showOKAlert(getResources().getString(R.string.message_invalid_coords));
			return;
		}

		double lat = 0;
		double lon = 0;

		try {
			lat = UnitsFormatting.parseCoord(poi_lat);
			lon = UnitsFormatting.parseCoord(poi_lon);

			lat = Utils.sanitizeLat(lat);
			lon = Utils.sanitizeLon(lon);
		}
		catch (ParseException e) {
			showOKAlert(getResources().getString(R.string.message_invalid_coords));
			return;
		}

		maybeReplacePOI(poi_title, lat, lon);
	}
}
