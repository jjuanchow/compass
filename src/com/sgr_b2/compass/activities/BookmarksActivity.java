package com.sgr_b2.compass.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.db.POIProvider;
import com.sgr_b2.compass.ui.POIAdapter;


public class BookmarksActivity extends Activity implements OnItemClickListener {

	private static final int MENU_ITEM_EDIT = 1;
	private static final int MENU_ITEM_DELETE = 2;
	private static final int MENU_ITEM_SEND = 3;

	private POIAdapter adapter = null;
	private POIProvider provider = null;

	private Location location = null;
	private POI selected_poi = null;

	public class Extras {
		public static final String LOCATION = "current_location";
		public static final String POI = "poi";
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		this.location = (Location)intent.getParcelableExtra(Extras.LOCATION);

		setContentView(R.layout.activity_bookmarks);
		setTitle(getResources().getString(R.string.title_bookmarks));

		this.provider = new POIProvider(this);
		this.adapter = new POIAdapter(this, provider);

		ListView pois = (ListView)findViewById(R.id.pois_layout);
		pois.setAdapter(this.adapter);
		pois.setOnItemClickListener(this);

		registerForContextMenu(pois);
	}

	@Override
	public void onDestroy() {
		if (this.provider != null) {
			this.provider.dispose();
		}

		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();

		this.adapter.update();

		findViewById(R.id.bookmarks_nothing_here).setVisibility(
			this.adapter.getCount() > 0 ?
			View.INVISIBLE : View.VISIBLE);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		Intent intent = new Intent();
		intent.putExtra(Extras.POI, (POI)this.adapter.getItem(position));

		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menu_info) {
		super.onCreateContextMenu(menu, v, menu_info);

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menu_info;

		POI poi = (POI)info.targetView.getTag();
		this.selected_poi = poi;

		menu.setHeaderTitle(poi.title);
		menu.add(0, MENU_ITEM_SEND, 0, R.string.menu_send_text);
		menu.add(0, MENU_ITEM_EDIT, 1, R.string.menu_edit_text);
		menu.add(0, MENU_ITEM_DELETE, 2, R.string.menu_delete_text);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_ITEM_SEND:
			ActivityUtils.sendPOI(this, this.selected_poi);
			break;

		case MENU_ITEM_EDIT:
			Intent intent = new Intent(this, AddEditActivity.class);
			intent.putExtra(AddEditActivity.Extras.MODE, AddEditActivity.MODE_EDIT);
			intent.putExtra(AddEditActivity.Extras.POI, this.selected_poi);

			startActivity(intent);
			break;

		case MENU_ITEM_DELETE:
			this.provider.deletePOI(this.selected_poi.title);
			this.adapter.update();
			break;
		}

		return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_bookmarks, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_add_bookmark: {
			Intent intent = new Intent(this, AddEditActivity.class);
			intent.putExtra(AddEditActivity.Extras.MODE, AddEditActivity.MODE_ADD);
			if (this.location != null) {
				intent.putExtra(AddEditActivity.Extras.LOCATION, this.location);
			}

			startActivity(intent);
			break;
		}

		case R.id.menu_import_bookmarks: {
			Intent i = new Intent(BookmarksActivity.this, ImportActivity.class);
			startActivity(i);
			break;
		}
		}

		return false;
	}
}
