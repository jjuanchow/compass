package com.sgr_b2.compass.activities;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationListener;
import android.location.LocationManager;


public class DeviceWrapper {

	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // meters
	public static final long MIN_TIME_BW_UPDATES = 1 * 1000; // msecs

	private LocationManager location_manager = null;
	private SensorManager sensors_manager = null;

	public DeviceWrapper(Context context) {
		this.location_manager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		this.sensors_manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	}

	/**
	 * check if GPS on this device is available/enabled
	 *
	 * @return true if GPS is available and enabled
	 */
	public boolean isGPSAvailable() {
		return (this.location_manager != null
			&& this.location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
	}

	/**
	 * enable location updates and setup listener
	 *
	 * @return true is setup was successful
	 */
	public boolean setupGPS(LocationListener listener) {
		if (!isGPSAvailable()) {
			return false;
		}

		this.location_manager.requestLocationUpdates(
			LocationManager.GPS_PROVIDER,
			MIN_TIME_BW_UPDATES,
			MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);

		return true;
	}

	/**
	 * turn off location updates
	 */
	public void releaseGPS(LocationListener listener) {
		if (this.location_manager != null && listener != null) {
			this.location_manager.removeUpdates(listener);
		}
	}

	/**
	 * check sensors needed for compass
	 *
	 * @return true if accelerometer and magnetic field sensors are available and enabled
	 */
	public boolean isCompassSensorsAvailable() {
		return (this.sensors_manager != null
			&& this.sensors_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null
			&& this.sensors_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null);
	}

	/**
	 * enable sensor updates and setup listener
	 *
	 * @return true on successful setup
	 */
	public boolean setupSensors(SensorEventListener listener) {
		if (!isCompassSensorsAvailable()) {
			return false;
		}

		Sensor accel = this.sensors_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		Sensor mag = this.sensors_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		this.sensors_manager.registerListener(listener, accel, SensorManager.SENSOR_DELAY_NORMAL);
		this.sensors_manager.registerListener(listener, mag, SensorManager.SENSOR_DELAY_NORMAL);

		return true;
	}

	/**
	 * disable sensor updates
	 */
	public void releaseSensors(SensorEventListener listener) {
		if (this.sensors_manager != null && listener != null) {
			this.sensors_manager.unregisterListener(listener);
		}
	}
}
