package com.sgr_b2.compass.activities;

import java.util.Arrays;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.filters.Screen;
import com.sgr_b2.compass.navigation.Monitor;
import com.sgr_b2.compass.navigation.MonitorEntry;
import com.sgr_b2.compass.navigation.Utils;
import com.sgr_b2.compass.themes.Theme;
import com.sgr_b2.compass.themes.ThemesFactory;
import com.sgr_b2.compass.ui.UnitsFormatting;


public class MainActivity extends Activity implements OnGlobalLayoutListener {

	private static final int REQUEST_BOOKMARK = 1;

	private static final long COMPASS_ANIMATION_DURATION = 1 * 500; // 0,5 second
	private static final int COMPASS_DIRECTIONS = 16; // number of compass directions
	private static final float ANIMATION_THRESHOLD = 360f / COMPASS_DIRECTIONS / 2;

	private int compass_center_x = 0;
	private int compass_center_y = 0;
	private Location location = null;
	private POI target = null;

	private float direction = 0;
	private int direction_state = 0; // used for graphics update

	private Monitor monitor = null;
	private Screen screen = new Screen(ANIMATION_THRESHOLD);
	private DeviceWrapper device_wrapper = null;
	private Menu menu = null; // this is here to manually update menu items on Android 1.x-2.x

	private Theme theme = null;

	private static boolean show_debug_info = false;
	private static boolean need_calibration = false;
	private static boolean close_to_magnet = false;
	private static boolean show_sensors_alert = true;
	private boolean show_waiting_location_alert = false;

	private int alert = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.device_wrapper= new DeviceWrapper(this);

		setContentView(R.layout.activity_main);
	}

	private void maybeShowCompassSensorsAlert() {
		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean dont_show_sensors_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_DONT_SHOW_SENSORS_ALERT, false);

		if (dont_show_sensors_alert) {
			return;
		}

		LayoutInflater layout_inflater = LayoutInflater.from(this);
		final View alert_view = layout_inflater.inflate(R.layout.sensors_alert, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				CheckBox checkbox = (CheckBox)alert_view.findViewById(R.id.dont_show);
				assert(checkbox != null);

				boolean dont_show = checkbox.isChecked();
				SharedPreferences.Editor editor = shared_pref.edit();
				editor.putBoolean(SettingsActivity.KEY_PREF_DONT_SHOW_SENSORS_ALERT, dont_show);
				editor.commit();
			}
		});
		builder.setMessage(R.string.message_no_magnetic_sensors);
		builder.setView(alert_view);
		builder.show();
	}

	@Override
	public void onResume() {
		super.onResume();

		this.monitor = new Monitor(this);
		this.monitor.setTarget(this.target != null ? this.target.getLocation() : null);
		this.monitor.execute((Object[])null);

		ImageView compass = (ImageView)findViewById(R.id.compass_image);
		ViewTreeObserver observer = compass.getViewTreeObserver();
		if (observer.isAlive()) {
			observer.addOnGlobalLayoutListener(this);
		}

		if (!this.device_wrapper.isCompassSensorsAvailable()
			&& show_sensors_alert) {
			maybeShowCompassSensorsAlert();
			show_sensors_alert = false;
		}

		this.device_wrapper.setupSensors(this.monitor);
		this.device_wrapper.setupGPS(this.monitor);

		SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		setupDebugInfo(shared_pref.getBoolean(SettingsActivity.KEY_PREF_DEBUG_INFO, false));

		this.show_waiting_location_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_WAITING_ALERT, false);

		this.theme = ThemesFactory.getTheme(this,
			shared_pref.getString(SettingsActivity.KEY_PREF_THEME,
				ThemesFactory.VANILLA));

		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	public void onPause() {
		super.onPause();

		this.device_wrapper.releaseGPS(this.monitor);
		this.device_wrapper.releaseSensors(this.monitor);

		if (this.monitor != null) {
			this.monitor.die();
			this.monitor = null;
		}
	}

	protected void onActivityResult(int request_code, int result_code, Intent data) {
		if (result_code != RESULT_OK) {
			return;
		}

		switch (request_code) {
		case REQUEST_BOOKMARK:
			POI poi = (POI)data.getSerializableExtra(BookmarksActivity.Extras.POI);
			assert(poi != null);

			updateTarget(poi);
			break;
		}

	}

	private void rotateCompass(float from, float to, long duration, int repeat_count) {
		float distance_ccw = Math.abs(to - from);
		float distance_cw = 360 - Math.abs(to - from);

		// always rotate compass using the shortest distance
		if (distance_cw < distance_ccw) {
			to = from + (to < from ? +distance_cw : -distance_cw);
		}

		RotateAnimation rotation = new RotateAnimation(from, to,
				this.compass_center_x, this.compass_center_y);
		rotation.setDuration(duration);
		rotation.setRepeatCount(repeat_count);
		rotation.setFillAfter(true);

		ImageView compass = (ImageView)findViewById(R.id.compass_image);
		compass.startAnimation(rotation);
	}

	private void setupDebugInfo(boolean show) {
		MainActivity.show_debug_info = show;

		int visibility = (show ? View.VISIBLE : View.INVISIBLE);

		findViewById(R.id.location_text).setVisibility(visibility);
		findViewById(R.id.target_location).setVisibility(visibility);
		findViewById(R.id.speed_text).setVisibility(visibility);
		findViewById(R.id.azimuth_text).setVisibility(visibility);
		findViewById(R.id.satellites_text).setVisibility(visibility);
	}

	private void showDirection(float azimuth) {
		if (!MainActivity.show_debug_info) {
			return;
		}

		TextView azimuth_text = (TextView)findViewById(R.id.azimuth_text);
		azimuth_text.setText(String.format(Locale.US, "%.0f", azimuth));
	}

	private void updateDirection(float azimuth) {
		// round azimuth to direction
		// otherwise constant compass rotation will be bad for a battery
		float new_direction = Utils.azimuthToDirection(this.screen.filter(azimuth), COMPASS_DIRECTIONS);

		if (this.direction == new_direction) {
			return;
		}

		rotateCompass(this.direction, new_direction, COMPASS_ANIMATION_DURATION, 0);

		this.direction = new_direction;
		showDirection(azimuth);
	}

	private void showSpeed(float mps) {
		if (!MainActivity.show_debug_info) {
			return;
		}

		TextView speed_text = (TextView)findViewById(R.id.speed_text);
		speed_text.setText(String.format(Locale.US, "%.2f", mps));
	}

	private void updateSpeed(float mps) {
		showSpeed(mps);
	}

	private void showDistance(float meters) {
		TextView distance_text = (TextView)findViewById(R.id.distance_text);
		distance_text.setText(UnitsFormatting.formatDistance(this, meters));
	}

	private void updateDistance(float meters) {
		if (this.target != null && this.location != null) {
			showDistance(meters);
		}
	}

	private void showLocation(Location location) {
		if (!MainActivity.show_debug_info) {
			return;
		}

		TextView location_text = (TextView)findViewById(R.id.location_text);
		location_text.setText(location == null ? null : UnitsFormatting.formatCoord(location.getLatitude())
			+ ", " + UnitsFormatting.formatCoord(location.getLongitude()));
	}

	private void updateLocation(Location location) {
		setLocation(location);
		showLocation(location);

		if (this.location != null && this.target != null) {
			updateDistance(this.location.distanceTo(this.target.getLocation()));
		}

		updateSendLocationMenuItem(this.menu);
	}

	private void showTarget(POI target) {
		String lat = UnitsFormatting.formatCoord(target.lat);
		String lon = UnitsFormatting.formatCoord(target.lon);

		TextView target_text = (TextView)findViewById(R.id.target_text);
		target_text.setText(target.title);

		TextView target_location = (TextView)findViewById(R.id.target_location);
		target_location.setText(lat + ", " + lon);
	}

	private void updateTarget(POI target) {
		// trigger this message only on target update
		// since app can work as compass w/o gps
		if (!device_wrapper.isGPSAvailable()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.message_gps_not_available);
			builder.setTitle(R.string.app_name);
			builder.setPositiveButton(android.R.string.ok, null);
			builder.create().show();

			return;
		}

		setTarget(target);

		if (this.target != null) {
			showTarget(target);
		}

		if (this.location != null && this.target != null) {
			updateDistance(this.location.distanceTo(this.target.getLocation()));
		}
	}

	private void setLocation(Location location) {
		this.location = location;
	}

	private void setTarget(POI target) {
		this.target = target;
	}

	private void showCalibrationAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton(android.R.string.ok, null);
		builder.setTitle(getTitle());
		builder.setMessage(R.string.message_magnetic_calibration);

		builder.show();
	}

	private void updateMagneticAccuracy(boolean unreliable, boolean direction_magnetic) {
		// only show alert if direction is magnetic
		if (unreliable && !MainActivity.need_calibration && direction_magnetic) {
			MainActivity.need_calibration = true;
			showCalibrationAlert();
		}
	}

	private void showCloseToMagnetAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton(android.R.string.ok, null);
		builder.setTitle(getTitle());
		builder.setMessage(R.string.message_magnetic_not_in_range);

		builder.show();
	}

	private void updateMagneticInRange(boolean in_range, boolean direction_magnetic) {
		// only show alert if direction is magnetic
		if (!in_range && !MainActivity.close_to_magnet && direction_magnetic) {
			MainActivity.close_to_magnet = true;
			showCloseToMagnetAlert();
		}
	}

	private void showSatellites(int satellites, int accuracy) {
		if (!MainActivity.show_debug_info) {
			return;
		}

		StringBuilder builder = new StringBuilder(String.valueOf(satellites));
		builder.append(" (");
		builder.append(String.valueOf(accuracy));
		builder.append(")");

		TextView speed_text = (TextView)findViewById(R.id.satellites_text);
		speed_text.setText(builder.toString());
	}

	private void updateSatellites(int satellites, int accuracy) {
		showSatellites(satellites, accuracy);
	}

	private void setAlertMessage(TextView view, int message) {
		this.alert = message;
		view.setText(this.alert < 0 ? null : getResources().getString(message));
	}

	private void selectAlertMessage(TextView view, MonitorEntry entry) {
		int preferred_message = -1;

		int[] alerts_priority = new int[]{
			R.string.alert_close_to_magnet,
			R.string.alert_need_calibration,
			R.string.alert_waiting_location,
		};

		// in order of priority
		if (entry.direction_is_magnetic && !entry.magnetic_in_range) {
			preferred_message = R.string.alert_close_to_magnet;
		}
		else if (entry.direction_is_magnetic && entry.magnetic_accuracy_low) {
			preferred_message = R.string.alert_need_calibration;
		}
		// for Stas
		else if (this.show_waiting_location_alert && entry.location == null) {
			preferred_message = R.string.alert_waiting_location;
		}

		// no preferred message
		if (preferred_message < 0) {
			setAlertMessage(view, preferred_message);
			return;
		}

		// if this is the same message - reset, to make it blink
		if (this.alert == preferred_message) {
			setAlertMessage(view, -1);
			return;
		}

		assert(this.alert > 0);
		assert(Arrays.asList(alerts_priority).indexOf(preferred_message) >= 0);

		// only set new alert if current alert is empty or has lower priority
		int current_priority = Arrays.asList(alerts_priority).indexOf(this.alert);
		int preferred_priority = Arrays.asList(alerts_priority).indexOf(preferred_message);

		// lower value -> higher priority -> do nothing
		if (current_priority >= 0 && current_priority < preferred_priority) {
			return;
		}

		setAlertMessage(view, preferred_message);
	}

	private void updateAlertMessage(MonitorEntry entry) {
		TextView alert_text = (TextView)findViewById(R.id.alert_text);
		assert(alert_text != null);

		selectAlertMessage(alert_text, entry);
	}

	private void updateTitle(boolean magnetic, boolean from_gps) {
		if (magnetic) {
			setTitle(R.string.title_magnetic_compass);
		}
		else if (from_gps) {
			setTitle(R.string.title_gps_compass);
		}
		else {
			setTitle(R.string.title_magnetic_compass_doesnt_work);
		}
	}

	private void updateGraphics(boolean magnetic, boolean from_gps) {
		int direction_state = (magnetic ? 1 : 0) * 2 + (from_gps ? 1 : 0);

		if (this.direction_state != direction_state) {
			ImageView compass = (ImageView)findViewById(R.id.compass_image);
			setupImaginery(from_gps, compass.getWidth(), compass.getHeight());
			this.direction_state = direction_state;
		}
	}

	public void updateStatus(MonitorEntry entry) {
		updateTitle(entry.direction_is_magnetic, entry.direction_from_gps);
		updateGraphics(entry.direction_is_magnetic, entry.direction_from_gps);

		updateDirection(entry.direction);
		updateDistance(entry.distance);

		updateLocation(entry.location);
		updateSpeed(entry.speed);
		updateSatellites(entry.gps_satellites, entry.gps_accuracy);

		updateMagneticAccuracy(entry.magnetic_accuracy_low, entry.direction_is_magnetic);
		updateMagneticInRange(entry.magnetic_in_range, entry.direction_is_magnetic);

		updateAlertMessage(entry);
	}

	private void showBookmarks() {
		Intent intent = new Intent(MainActivity.this, BookmarksActivity.class);
		if (this.location != null) {
			intent.putExtra(BookmarksActivity.Extras.LOCATION, this.location);
		}

		startActivityForResult(intent, REQUEST_BOOKMARK);
	}

	private void setupImaginery(boolean from_gps, int width, int height) {
		ImageView compass_arrow = (ImageView)findViewById(R.id.compass_image);
		ImageView compass_bg = (ImageView)findViewById(R.id.compass_bg);

		compass_arrow.setImageDrawable(from_gps
			? this.theme.getGPSCompassArrow(width, height)
			: this.theme.getMagneticCompassArrow(width, height));

		compass_bg.setImageDrawable(from_gps
			? this.theme.getGPSCompassBG(width, height)
			: this.theme.getMagneticCompassBG(width, height));
	}

	@Override
	public void onGlobalLayout() {
		ImageView compass = (ImageView)findViewById(R.id.compass_image);

		this.compass_center_x = compass.getWidth() / 2;
		this.compass_center_y = compass.getHeight() / 2;

		setupImaginery(false, compass.getWidth(), compass.getHeight());

		// unsubscribe from updates
		ViewTreeObserver observer = compass.getViewTreeObserver();
		if (observer.isAlive()) {
			observer.removeGlobalOnLayoutListener(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		this.menu = menu;

		return true;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		this.menu = null;
	}

	private void updateSendLocationMenuItem(Menu menu) {
		if (menu != null) {
			menu.findItem(R.id.menu_send_location).setEnabled(this.location != null);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		updateSendLocationMenuItem(menu);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_bookmarks:
			showBookmarks();
			break;

		case R.id.menu_send_location:
			if (this.location != null) {
				ActivityUtils.sendLocation(this, this.location);
			}
			break;

		case R.id.menu_settings:
			Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
			startActivity(intent);
			break;
		}

		return false;
	}
}
