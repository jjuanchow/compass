package com.sgr_b2.compass.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.sgr_b2.compass.R;


public class SettingsActivity extends PreferenceActivity {

	public static final String KEY_PREF_UNITS = "pref_display_units";
	public static final String KEY_PREF_THEME = "pref_theme";
	public static final String KEY_PREF_SHOW_WAITING_ALERT = "pref_show_waiting_alert";
	public static final String KEY_PREF_DEBUG_INFO = "pref_show_debug";
	public static final String KEY_PREF_DONT_SHOW_SENSORS_ALERT = "pref_dont_show_sensors_alert";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);
	}
}
