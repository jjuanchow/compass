package com.sgr_b2.compass.db;

import java.io.Serializable;

import android.location.Location;


public class POI implements Serializable {

	private static final long serialVersionUID = -7560795199573367237L;

	public int id;
	public String title;
	public double lat;
	public double lon;

	public POI(double lat, double lon, String title) {
		this.id = -1;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}

	public POI(double lat, double lon, String title, int id) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}

	public POI(Location location, String title) {
		this.lat = location.getLatitude();
		this.lon = location.getLongitude();
		this.title = title;
	}

	public Location getLocation() {
		Location loc = new Location("noone");
		loc.setLatitude(this.lat);
		loc.setLongitude(this.lon);
		return loc;
	}

	public static String getTitle(POI poi, String backup) {
		return (poi.title == null ? backup : poi.title);
	}
}
