package com.sgr_b2.compass.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;


public class POIProvider {
	private DBHelper helper = null;
	private SQLiteDatabase db = null;

	public POIProvider(Context context) {
		this.helper = new DBHelper(context);
		this.db = this.helper.getWritableDatabase();
	}

	public synchronized void dispose() {
		if (this.db != null) {
			this.db.close();
		}
	}

	public synchronized long getCount() {
		return DatabaseUtils.queryNumEntries(this.db, DBHelper.TABLE_POIS);
	}

	public synchronized POI getPOI(int position) {
		String columns[] = {
				"rowid",
				"lat", "lon",
				"title",
		};

		Cursor cursor = this.db.query(DBHelper.TABLE_POIS, columns, null, null, null, null, null);

		try {
			cursor.moveToPosition(position);
			int id = cursor.getInt(0);
			double lat = cursor.getDouble(1);
			double lon = cursor.getDouble(2);
			String title = cursor.getString(3);

			return new POI(lat, lon, title, id);
		}
		finally {
			cursor.close();
		}
	}

	public synchronized POI hasPOI(final String title) {
		String columns[] = {
				"rowid",
				"lat", "lon",
		};

		Cursor cursor = this.db.query(DBHelper.TABLE_POIS,
				columns,
				"title='" + title + "'",
				null, null, null, null);

		try {
			if (cursor.getCount() < 1) {
				return null;
			}

			cursor.moveToFirst();
			int id = cursor.getInt(0);
			double lat = cursor.getDouble(1);
			double lon = cursor.getDouble(2);

			return new POI(lat, lon, title, id);
		}
		finally {
			cursor.close();
		}
	}

	public synchronized void addPOI(POI poi) {
		ContentValues values = new ContentValues();
		values.put("lat", poi.lat);
		values.put("lon", poi.lon);
		values.put("title", poi.title);

		this.db.insert(DBHelper.TABLE_POIS, null, values);
	}

	public synchronized void deletePOI(String title) {
		this.db.delete(DBHelper.TABLE_POIS, "title='" + title + "'", null);
	}

	public synchronized void updatePOI(POI poi) {
		ContentValues values = new ContentValues();
		values.put("lat", poi.lat);
		values.put("lon", poi.lon);
		values.put("title", poi.title);

		this.db.update(DBHelper.TABLE_POIS, values, "title='" + poi.title + "'", null);
	}
}
