package com.sgr_b2.compass.filters;


public class LowPass {

	/**
	 * low pass filter for two floats
	 *
	 * http://en.wikipedia.org/wiki/Low-pass_filter
	 */
	public static float filter(final float input, final float output, final float alpha) {
		return output + alpha * (input - output);
	}
}
