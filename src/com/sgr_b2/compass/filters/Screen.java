package com.sgr_b2.compass.filters;

public class Screen {

	private float threshold = 0;
	private float current = 0;

	public Screen(float threshold) {
		this.threshold = threshold;
	}

	/**
	 * this filter is supposed to screen changes below threshold value
	 * note that it operates on circular range of 0..359
	 * therefore distance between 0 and 359 is  equal to 1
	 *
	 * @param current current value
	 * @return previous value if change is below the threshold
	 */
	public float filter(float current) {
		double cw_difference = Math.abs(current - this.current);
		double ccw_difference = 360 - Math.abs(current - this.current);

		if (cw_difference < this.threshold || ccw_difference < this.threshold) {
			return this.current;
		}

		this.current = current;
		return current;
	}
}
