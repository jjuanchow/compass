package com.sgr_b2.compass.io;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.ui.UnitsFormatting;


/**
 * ♡
 */
public class HRTProtocol {
	private final static Pattern location_regex = Pattern.compile("(.*?)?([-+]?[0-9]+\\.?[0-9]{0,6}),([-+]?[0-9]+\\.?[0-9]{0,6})",
		Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	/**
	 * build POI out of HRT text, maybe multiline
	 */
	public static POI unmarshall(final String text) {
		for (String line : text.split("\n")) {
			Matcher location_matcher = HRTProtocol.location_regex.matcher(line);
			if (location_matcher.matches()) {
				String title = location_matcher.group(1);
				Double lat = Double.valueOf(location_matcher.group(2));
				Double lon = Double.valueOf(location_matcher.group(3));

				if (title != null) {
					title = title.trim();
				}

				if (title.length() < 1) {
					title = null;
				}

				return new POI(lat, lon, title);
			}
		}

		return null;
	}

	/**
	 * write POI into HRT text
	 */
	public static String marshall(final POI poi) {
		StringBuilder builder = new StringBuilder();

		if (poi.title != null && poi.title.length() > 0) {
			builder.append(poi.title);
			builder.append(" ");
		}

		builder.append(UnitsFormatting.formatCoord(poi.lat));
		builder.append(",");
		builder.append(UnitsFormatting.formatCoord(poi.lon));

		return builder.toString();
	}
}
