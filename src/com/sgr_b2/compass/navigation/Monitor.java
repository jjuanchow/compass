package com.sgr_b2.compass.navigation;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;

import com.sgr_b2.compass.activities.ImportActivity;
import com.sgr_b2.compass.activities.MainActivity;
import com.sgr_b2.compass.db.LogProvider;
import com.sgr_b2.compass.sms.SMSPOI;
import com.sgr_b2.compass.sms.SMSReader;


public class Monitor extends AsyncTask<Object, MonitorEntry, Object>
	implements SensorEventListener, LocationListener {

	private final long SMS_CHECK_INTERVAL = 10; // seconds
	private final float ACCURACY_THRESHOLD = 50; // meters with 68% probability

	private MainActivity main;
	private boolean die = false;
	private Router router = new Router();

	private float[] gravity = null;
	private float[] geomagnetic = null;
	private float[] r_matrix = new float[16];
	private float[] i_matrix = new float[16];

	private SMSReader sms_reader = null;
	private LogProvider log_provider = null;

	public Monitor(MainActivity main) {
		this.main = main;
		this.die = false;
		this.sms_reader = new SMSReader(main);
		this.log_provider = new LogProvider(main);
	}

	public void die() {
		this.die = true;
	}

	public void setTarget(Location target) {
		this.router.setTarget(target);
	}

	/**
	 * check SMS messages, running in BG thread
	 */
	private boolean checkTheTweets() {
		if (this.sms_reader == null || this.log_provider == null) {
			return false;
		}

		String last_recorded_sms_id = this.log_provider.getLastSMSId();
		String last_actual_sms_id = this.sms_reader.getLastSMSId();

		if (last_recorded_sms_id != null
			&& last_recorded_sms_id.equals(last_actual_sms_id)) {
			return false;
		}

		final SMSPOI poi = this.sms_reader.findLocation(last_recorded_sms_id,
			SMSReader.HISTORY_DEFAULT);

		this.log_provider.setLastSMSId(last_actual_sms_id);

		return (poi != null);
	}

	@Override
	protected Object doInBackground(Object... params) {
		long tick = 0;

		while (!this.die) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			long now = System.currentTimeMillis();

			MonitorEntry entry = new MonitorEntry();

			this.router.selectHeading(entry, now);
			this.router.selectMagneticExtras(entry, now);
			this.router.selectLocation(entry, now);
			this.router.selectSpeed(entry, now);
			this.router.selectDirection(entry, now);
			this.router.selectLocationExtras(entry, now);

			publishProgress(entry);

			boolean tweets = false;

			if (tick % SMS_CHECK_INTERVAL == 0) {
				tweets = checkTheTweets(); // und emails
			}

			if (tweets) {
				Intent intent = new Intent(this.main, ImportActivity.class);
				this.main.startActivity(intent);
			}

			++tick; // no worries about overflow, it will still work
		}

		this.log_provider.dispose();

		return null;
	}

	protected void onProgressUpdate(MonitorEntry... progress) {
		for (MonitorEntry entry : progress) {
			this.main.updateStatus(entry);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		synchronized (this) {

		}
	}

	private int androidToRouterAccuracy(int accuracy) {
		switch (accuracy) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			return Router.MAG_ACCURACY_HIGH;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			return Router.MAG_ACCURACY_NORMAL;
		case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
			return Router.MAG_ACCURACY_LOW;
		default:
			return Router.MAG_ACCURACY_NONE;
		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				this.gravity = event.values.clone();
			}

			if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				this.geomagnetic = event.values.clone();
				this.router.setExtra(Router.EXTRA_MAG_ACCURACY,
					androidToRouterAccuracy(event.accuracy));
				this.router.setExtra(Router.EXTRA_MAG_IN_RANGE,
					(Utils.magneticFieldValid(
						Utils.magneticFieldStrength(this.geomagnetic)) ? 1 : 0));
			}

			if (this.gravity == null || this.geomagnetic == null) {
				return;
			}

			SensorManager.getRotationMatrix(this.r_matrix, this.i_matrix,
				this.gravity, this.geomagnetic);

			float[] orientation = new float[3];
			SensorManager.getOrientation(this.r_matrix, orientation);

			float azimuth = orientation[0] * (float)(180.0f / Math.PI);

			this.router.updateMagneticHeading(azimuth);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		synchronized (this) {
			if (location != null) {
				if (!location.hasAccuracy()) {
					return;
				}

				this.router.setExtra(Router.EXTRA_GPS_ACCURACY, (int)location.getAccuracy());

				if (location.getAccuracy() > ACCURACY_THRESHOLD) {
					return;
				}
			}

			this.router.updateLocation(location, System.currentTimeMillis());
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		synchronized (this) {
			this.router.updateLocation(null, System.currentTimeMillis());
		}
	}

	@Override
	public void onProviderEnabled(String provider) {
		synchronized (this) {

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		synchronized (this) {
			switch (status) {
			case LocationProvider.OUT_OF_SERVICE:
			case LocationProvider.TEMPORARILY_UNAVAILABLE:
				this.router.updateLocation(null, System.currentTimeMillis());
				this.router.setExtra(Router.EXTRA_GPS_ACCURACY, 0);
				break;
			default:
				if (extras != null) {
					this.router.setExtra(Router.EXTRA_SATELLITES,
						extras.getInt("satellites", 0));
				}
			}
		}
	}
}
