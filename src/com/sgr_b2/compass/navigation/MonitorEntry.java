package com.sgr_b2.compass.navigation;

import android.location.Location;


public class MonitorEntry {
	// tracker stats
	public float azimuth;
	public float distance;
	public float direction;
	public boolean direction_is_magnetic;
	public boolean direction_from_gps;
	public float speed;
	public Location location;

	// sensor stats
	public boolean magnetic_accuracy_low;
	public boolean magnetic_in_range;
	public int gps_satellites;
	public int gps_accuracy;

	public MonitorEntry() {
		azimuth = 0;
		distance = 0;
		direction = 0;
		direction_is_magnetic = false;
		direction_from_gps = false;
		speed = 0;
		location = null;

		magnetic_accuracy_low = false;
		magnetic_in_range = true;
		gps_satellites = 0;
		gps_accuracy = 0;
	}
}
