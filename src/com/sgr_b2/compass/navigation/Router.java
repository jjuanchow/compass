package com.sgr_b2.compass.navigation;

import android.location.Location;


public class Router {

	public static final int EXTRA_SATELLITES = 0;
	public static final int EXTRA_MAG_ACCURACY = 1;
	public static final int EXTRA_MAG_IN_RANGE = 2;
	public static final int EXTRA_GPS_ACCURACY = 3;

	public static final int MAG_ACCURACY_NONE = 0;
	public static final int MAG_ACCURACY_LOW = 1;
	public static final int MAG_ACCURACY_NORMAL = 2;
	public static final int MAG_ACCURACY_HIGH = 3;

	public static final long LOCATION_TTL = 10 * 1000; // msecs
	public static final long SPEED_TTL = 2 * 1000; // msecs

	private Tracker tracker = null;
	private Location target = null;
	private long location_update_ms = 0;

	private int magnetic_accuracy = MAG_ACCURACY_HIGH;
	private boolean magnetic_in_range = true;
	private int gps_satellites = 0;
	private int gps_accuracy = 0;

	public Router() {
		this.tracker = new Tracker();
	}

	public synchronized void setTarget(Location target) {
		this.target = (target != null ? new Location(target) : null);
	}

	public synchronized void setExtra(int extra, int value) {
		switch (extra) {
		case EXTRA_SATELLITES:
			this.gps_satellites = value;
			break;
		case EXTRA_MAG_ACCURACY:
			this.magnetic_accuracy = value;
			break;
		case EXTRA_MAG_IN_RANGE:
			this.magnetic_in_range = (value != 0 ? true : false);
			break;
		case EXTRA_GPS_ACCURACY:
			this.gps_accuracy = value;
			break;
		default:
			assert(false);
		}
	}

	public synchronized void selectHeading(MonitorEntry entry, long now) {
		entry.azimuth = 0;
		entry.direction_is_magnetic = false;
		entry.direction_from_gps = false;

		if (this.tracker.hasLocationHeading()
			&& now - this.location_update_ms < LOCATION_TTL) {
			entry.azimuth = this.tracker.getLocationHeading();
			entry.direction_from_gps = true;
		}
		else if (tracker.hasMagneticHeading()) {
			entry.azimuth = this.tracker.getMagneticHeading();
			entry.direction_is_magnetic = true;
		}
	}

	public synchronized void selectSpeed(MonitorEntry entry, long now) {
		entry.speed = 0;

		if (this.tracker.hasSpeed()
			&& now - this.location_update_ms < SPEED_TTL) {
			entry.speed = this.tracker.getSpeed();
		}
	}

	public synchronized void selectMagneticExtras(MonitorEntry entry, long now) {
		switch (this.magnetic_accuracy) {
		case MAG_ACCURACY_HIGH:
		case MAG_ACCURACY_NORMAL:
			entry.magnetic_accuracy_low = false;
			break;

		default:
			entry.magnetic_accuracy_low = true;
		}

		entry.magnetic_in_range = this.magnetic_in_range;
	}

	public synchronized void selectLocation(MonitorEntry entry, long now) {
		entry.location = this.tracker.getLocation();
	}

	public synchronized void selectDirection(MonitorEntry entry, long now) {
		if (this.target != null) {
			entry.direction = this.tracker.getDirection(entry.azimuth, this.target);
			entry.distance = this.tracker.getDistance(this.target);
		}
		else {
			entry.direction = Utils.normalizeAzimuth(-entry.azimuth);
		}
	}

	public synchronized void selectLocationExtras(MonitorEntry entry, long now) {
		entry.gps_satellites = this.gps_satellites;
		entry.gps_accuracy = this.gps_accuracy;
	}

	public synchronized void updateLocation(Location location, long now) {
		this.tracker.updateLocation(location, now);
		this.location_update_ms = (location == null ? 0 : now);

		if (location == null) {
			setExtra(EXTRA_SATELLITES, 0);
		}
	}

	public synchronized void updateMagneticHeading(float azimuth) {
		this.tracker.updateMagneticHeading(azimuth);
	}
}
