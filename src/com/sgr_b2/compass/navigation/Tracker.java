package com.sgr_b2.compass.navigation;

import android.location.Location;

import com.sgr_b2.compass.filters.LowPass;


public class Tracker {

	public static final float ANGLE_ALPHA = 0.15f;
	public static final float SPEED_ALPHA = 0.15f;

	public static final long LOCATION_READINGS_THRESHOLD = 5; // how many location readings to skip before taking bearing from them

	private Location last_location = null;

	private boolean has_magnetic_heading = false;
	private float magnetic_azimuth = 0;

	private boolean has_location_heading = false;
	private long location_readings = 0;
	private float location_azimuth = 0;

	private boolean has_speed = false;
	private float speed = 0;

	/**
	 * get direction to target
	 *
	 * @param azimuth magnetic or GPS azimuth
	 * @param target
	 * @return direction angle in 0..359
	 */
	public synchronized float getDirection(float heading, Location target) {
		Location location = getLocation();
		if (location == null || target == null) {
			return 0;
		}

		float target_direction = Utils.normalizeAzimuth(Utils.direction(location, target));

		return (float)(int)Utils.normalizeAzimuth(target_direction - heading);
	}

	public synchronized boolean hasLocationHeading() {
		return this.has_location_heading;
	}

	/**
	 * get azimuth as calculated by GPS
	 *
	 * @return azimuth in 0..359
	 */
	public synchronized float getLocationHeading() {
		return (float)(int)this.location_azimuth;
	}

	/**
	 * @param azimuth angle between north and target (phone head)
	 */
	private synchronized void updateLocationHeading(float azimuth, long now) {
		float fixed_azimuth = Utils.normalizeAzimuth(azimuth);
		this.location_azimuth = LowPass.filter(this.location_azimuth,
			fixed_azimuth, ANGLE_ALPHA);
	}

	public synchronized boolean hasMagneticHeading() {
		return this.has_magnetic_heading;
	}

	/**
	 * get magnetic azimuth
	 *
	 * @return azimuth in 0..359
	 */
	public synchronized float getMagneticHeading() {
		return (float)(int)this.magnetic_azimuth;
	}

	/**
	 * @param azimuth angle between north and target (phone head)
	 */
	public synchronized void updateMagneticHeading(float azimuth) {
		float fixed_azimuth = Utils.normalizeAzimuth(azimuth);
		this.magnetic_azimuth = LowPass.filter(this.magnetic_azimuth,
			fixed_azimuth, ANGLE_ALPHA);

		this.has_magnetic_heading = true;
	}

	/**
	 * @return last available location if any
	 */
	public synchronized Location getLocation() {
		return this.last_location;
	}

	private synchronized void updateSpeed(float speed, long now) {
		this.speed = LowPass.filter(this.speed, speed, SPEED_ALPHA);
	}

	/**
	 * @param location
	 */
	public synchronized  void updateLocation(Location location, long now) {
		this.last_location = (location != null ? new Location(location) : null);

		this.has_speed = (location != null && location.hasSpeed());
		if (this.has_speed) {
			updateSpeed(location.getSpeed(), now);
		}

		boolean has_location_heading = (location != null && location.hasBearing());
		if (has_location_heading) {
			// skip some number of location updates
			// they usually have bearing, but it's wrong
			if (this.location_readings < LOCATION_READINGS_THRESHOLD) {
				++this.location_readings;
			}
			else {
				this.has_location_heading = true;
				updateLocationHeading(location.getBearing(), now);
			}
		}
	}

	/**
	 * calculate geodesic distance
	 *
	 * @param target
	 * @return distance to @target in meters
	 */
	public synchronized float getDistance(Location target) {
		Location location = getLocation();
		if (location != null && target != null) {
			return location.distanceTo(target);
		}

		return 0;
	}

	/**
	 * @return true if speed was updated recently enough
	 */
	public synchronized boolean hasSpeed() {
		return this.has_speed;
	}

	/**
	 * @return speed in m/s or 0 if location is too old
	 */
	public synchronized float getSpeed() {
		return this.speed;
	}
}
