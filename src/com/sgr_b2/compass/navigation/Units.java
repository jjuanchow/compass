package com.sgr_b2.compass.navigation;


public class Units {
	/**
	 * meters per second to kilometers per hour
	 *
	 * @return speed in km/h
	 */
	public static float mps2kmph(float mps) {
		return (mps / (1000.0f / 60 / 60));
	}

	/**
	 * kilometers per hour to meters per seconds
	 *
	 * @return speed in m/s
	 */
	public static float kmph2mps(float kmph) {
		return ((kmph * 1000) / 60 / 60);
	}

	/**
	 * meters to feet
	 */
	public static float m2ft(float m) {
		return m * 3.28084f;
	}

	/**
	 * kilometers to miles
	 */
	public static float km2mi(float km) {
		return km * 0.621371f;
	}

	/**
	 * meters to kilometers.
	 *
	 * ok, this is stupid, but it better to have this along with @see feet2miles.
	 */
	public static float m2km(float m) {
		return m / 1000;
	}

	/**
	 * feet to miles
	 */
	public static float ft2mi(float feet) {
		return feet / 5280;
	}
}
