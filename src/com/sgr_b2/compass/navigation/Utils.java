package com.sgr_b2.compass.navigation;

import android.location.Location;


public class Utils {
	/**
	 * calculate angle to target
	 *
	 * @return angle in 0..359
	 */
	public static float direction(Location here, Location there) {
		return (float)Math.atan2((there.getLongitude() - here.getLongitude()),
				(there.getLatitude() - here.getLatitude())) * (float)(180.0f / Math.PI);
	}

	/**
	 * @return speed in m/s
	 */
	public static float speed(float meters, long miliseconds) {
		assert(miliseconds > 0);

		return meters / (miliseconds / 1000f);
	}

	/**
	 * @return lat in range -90..90
	 */
	public static double sanitizeLat(double lat) {
		return (lat < -90 ? -90 : (
			lat > 90 ? 90 : lat));
	}

	/**
	 * @return lon in range -180..180
	 */
	public static double sanitizeLon(double lon) {
		return (lon < -180 ? -180 : (
			lon > 180 ? 180 : lon));
	}

	/**
	 * covert negative angle into positive, fix overflow, etc
	 *
	 * @return azimuth in 0..360
	 */
	public static float normalizeAzimuth(float azimuth) {
		// bring value to 0..-359
		if (azimuth < 0) {
			azimuth %= -360;
		}

		// bring value to 0..359
		if (azimuth >= 360) {
			azimuth %= 360;
		}

		// convert value to positive
		if (azimuth < 0) {
			azimuth += 360;
		}

		return azimuth;
	}

	/**
	 * transform angle to scale of directions. for instance,
	 * 4 directions:
	 *
	 *          0
	 *          ^
	 *          |
	 *          |
	 * 270 <----o----> 90
	 *          |
	 *          |
	 *          v
	 *         180
	 *
	 * 44 degrees will be transformed into 0 degrees
	 * 50 degrees will be transformed into 90 degrees
	 * and so
	 *
	 * @return basically angle rounded to most appropriate direction
	 */
	public static float azimuthToDirection(float azimuth, int directions) {
		assert(directions > 0);

		return ((int)(azimuth / (360.0f / directions))) * (360f / directions);
	}

	/**
	 * calculate absolute magnetic field value
	 *
	 * @param values magnetic field strength in x, y, z axis
	 * @return absolute magnetic field strength
	 */
	public static double magneticFieldStrength(float[] values) {
		assert(values.length >= 3);

		return Math.sqrt(values[0] * values[0]
			+ values[1] * values[1]
			+ values[2] * values[2]);
	}

	/**
	 * check if magnetic field strength is in reasonable range
	 *
	 * @param strength magnetic field strength, uT
	 * @return true if it's ok
	 */
	public static boolean magneticFieldValid(double strength) {
		return (20 <= strength && strength <= 70);
	}
}
