package com.sgr_b2.compass.themes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

import com.sgr_b2.compass.R;


public class MinimalTheme extends Theme {

	Context context;

	public MinimalTheme(Context context) {
		this.context = context;
	}

	@Override
	public BitmapDrawable getGPSCompassBG(int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(this.context.getResources(),
			R.drawable.compass_bg_gps_minimal);

		return GraphicUtils.scaleBitmap(bmp, width, height);
	}

	@Override
	public BitmapDrawable getGPSCompassArrow(int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(this.context.getResources(),
			R.drawable.compass_arrow_minimal);

		return GraphicUtils.scaleBitmap(bmp, width, height);
	}

	@Override
	public BitmapDrawable getMagneticCompassBG(int width, int height) {
		return null;
	}

	@Override
	public BitmapDrawable getMagneticCompassArrow(int width, int height) {
		return getGPSCompassArrow(width, height);
	}
}
