package com.sgr_b2.compass.ui;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.activities.SettingsActivity;
import com.sgr_b2.compass.navigation.Units;


public class UnitsFormatting {

	private final static DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols();
	private final static DecimalFormat COORD_FORMAT = new DecimalFormat("0.000000");
	private final static DecimalFormat KILOMETERS_DISTANCE_FORMAT = new DecimalFormat("0.#");
	private final static DecimalFormat METERS_DISTANCE_FORMAT = new DecimalFormat("0");

	private static boolean displayImperialUnits(Context context) {
		SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(context);
		String pref_units = shared_pref.getString(SettingsActivity.KEY_PREF_UNITS, "m");

		return !pref_units.equals("m");
	}

	/**
	 * format coordinate into common internal (text) representation
	 * this remove ambiguity like decimal point symbol '.' or ',' and so
	 *
	 * @param c coordinate to format
	 * @return text representation of coordinate
	 */
	public static String formatCoord(double c) {
		UnitsFormatting.SYMBOLS.setDecimalSeparator('.');
		UnitsFormatting.COORD_FORMAT.setDecimalFormatSymbols(UnitsFormatting.SYMBOLS);

		return UnitsFormatting.COORD_FORMAT.format(c);
	}

	/**
	 * parse text representation of coordinate into double
	 * it doesn't matter what decimal point delimiter used and so
	 *
	 * @param c coordinate text representation
	 * @return double value
	 * @throws ParseException
	 */
	public static double parseCoord(final String c) throws ParseException {
		String c_modified = c.replace(',', '.');

		UnitsFormatting.COORD_FORMAT.setDecimalFormatSymbols(UnitsFormatting.SYMBOLS);
		UnitsFormatting.COORD_FORMAT.setDecimalFormatSymbols(UnitsFormatting.SYMBOLS);

		return UnitsFormatting.COORD_FORMAT.parse(c_modified).doubleValue();
	}

	/**
	 * format distance into text, shortest possible representation
	 * it will automatically scale meters to kilometers and feet to miles
	 *
	 * @return text suitable for UI
	 */
	public static String formatDistance(Context context, float d) {
		float scaled = Units.m2km(d);
		String units = context.getResources().getString(R.string.m);
		String scaled_units = context.getResources().getString(R.string.km);

		boolean imperial = displayImperialUnits(context);
		if (imperial) {
			d = Units.m2ft(d);
			scaled = Units.ft2mi(d);

			units = context.getResources().getString(R.string.ft);
			scaled_units = context.getResources().getString(R.string.mi);
		}

		UnitsFormatting.SYMBOLS.setDecimalSeparator('.');

		DecimalFormat formatter = (scaled >= 1 ? KILOMETERS_DISTANCE_FORMAT : METERS_DISTANCE_FORMAT);
		formatter.setDecimalFormatSymbols(UnitsFormatting.SYMBOLS);

		return String.format(Locale.US, "%s %s",
			formatter.format((scaled >= 1 ? scaled : d)),
			(scaled >= 1 ? scaled_units : units));
	}

	/**
	 * format location according to user preferences
	 *
	 * @return text suitable for UI
	 */
	public static String formatLocation(Context context, double lat, double lon) {
		StringBuilder builder = new StringBuilder();

		builder.append(UnitsFormatting.formatCoord(lat));
		builder.append(", ");
		builder.append(UnitsFormatting.formatCoord(lon));

		return builder.toString();
	}
}
