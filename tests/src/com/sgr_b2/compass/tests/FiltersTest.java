package com.sgr_b2.compass.tests;


import junit.framework.TestCase;

import com.sgr_b2.compass.filters.LowPass;
import com.sgr_b2.compass.filters.Screen;


public class FiltersTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testFloatsLowPass() {
		assertEquals(0,  LowPass.filter(0, 0, 0.15f), 0.01f);
		assertEquals(1 * 0.85f,  LowPass.filter(0, 1, 0.15f), 0.01f);
		assertEquals(0.5 * 0.85f,  LowPass.filter(0, 0.5f, 0.15f), 0.01f);
	}

	public void testScreen() {
		Screen screen = new Screen(10);

		assertEquals(0, screen.filter(0), 0.01f);
		assertEquals(0, screen.filter(1), 0.01f);
		assertEquals(0, screen.filter(9), 0.01f);
		assertEquals(10, screen.filter(10), 0.01f); // over threshold
		assertEquals(10, screen.filter(11), 0.01f);
		assertEquals(10, screen.filter(19), 0.01f);
		assertEquals(55, screen.filter(55), 0.01f); // significantly over threshold
		assertEquals(55, screen.filter(64), 0.01f); // under threshold again
		assertEquals(65, screen.filter(65), 0.01f); // over threshold
		assertEquals(65, screen.filter(65), 0.01f); // nothing changed
		assertEquals(85, screen.filter(85), 0.01f); // over threshold
		assertEquals(105, screen.filter(105), 0.01f); // over threshold

		// negative
		assertEquals(-2, screen.filter(-2), 0.01f); // over threshold
		assertEquals(-2, screen.filter(-3), 0.01f);
		assertEquals(-2, screen.filter(-11), 0.01f);
		assertEquals(-12, screen.filter(-12), 0.01f); // over threshold
		assertEquals(0, screen.filter(0), 0.01f); // over threshold
	}

	public void testScreenCircularRange() {
		Screen screen = new Screen(10);
		assertEquals(0, screen.filter(0), 0.01f);
		assertEquals(0, screen.filter(3), 0.01f); // below the range
		assertEquals(0, screen.filter(357), 0.01f); // below the range

		assertEquals(349, screen.filter(349), 0.01f); // over threshold
	}
}
