package com.sgr_b2.compass.tests;


import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.io.HRTProtocol;

import junit.framework.TestCase;

public class HRTProtocolTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testUnmarshalling() {
		{
			POI poi = HRTProtocol.unmarshall("привет ромашки 1.200000,3.400000");
			assertEquals("привет ромашки", poi.title);
			assertTrue(Math.abs(poi.lat - 1.2) < 0.01);
			assertTrue(Math.abs(poi.lon - 3.4) < 0.01);
		}

		{
			POI poi = HRTProtocol.unmarshall(" 1.200000,3.400000");
			assertTrue(poi.title == null);
			assertTrue(Math.abs(poi.lat - 1.2) < 0.01);
			assertTrue(Math.abs(poi.lon - 3.4) < 0.01);
		}
	}

	public void testMarshalling() {
		// pois w/o title
		assertEquals("0.000000,0.000000", HRTProtocol.marshall(new POI(0, 0, null)));
		assertEquals("0.100000,-0.200000", HRTProtocol.marshall(new POI(0.1f, -0.2f, null)));

		// poi w/title
		assertEquals("привет 0.300000,0.400000", HRTProtocol.marshall(new POI(0.3f, 0.4f, "привет")));
	}

	public void testMultistring() {
		{
			POI poi = HRTProtocol.unmarshall("Hey,\n" +
				"what's up 1.200000,-3.400000");
			assertEquals("what's up", poi.title);
			assertEquals(poi.lat, 1.2f, 0.000001);
			assertEquals(poi.lon, -3.4f, 0.000001);
		}
	}

	public void testParsingPositives() {
		// no decimal part
		{
			POI poi = HRTProtocol.unmarshall("1,2");
			assertTrue(poi != null);
			assertEquals(poi.lat, 1, 0.000001);
			assertEquals(poi.lon, 2, 0.000001);
		}

		// no decimal part, but still valid
		{
			POI poi = HRTProtocol.unmarshall("1.,2.");
			assertTrue(poi != null);
			assertEquals(poi.lat, 1, 0.000001);
			assertEquals(poi.lon, 2, 0.000001);
		}

		// shortest decimal part
		{
			POI poi = HRTProtocol.unmarshall("1.0,2.0");
			assertTrue(poi != null);
			assertEquals(poi.lat, 1, 0.000001);
			assertEquals(poi.lon, 2, 0.000001);
		}

		// longest decimal part
		{
			POI poi = HRTProtocol.unmarshall("1.000000,2.000000");
			assertTrue(poi != null);
			assertEquals(poi.lat, 1, 0.000001);
			assertEquals(poi.lon, 2, 0.000001);
		}

		// too long decimal part
		{
			POI poi = HRTProtocol.unmarshall("1.0000000,2.0000000");
			assertTrue(poi == null);
		}
	}

	public void testParsingNegatives() {
		// no decimal part
		{
			POI poi = HRTProtocol.unmarshall("-1,-2");
			assertTrue(poi != null);
			assertEquals(poi.lat, -1, 0.000001);
			assertEquals(poi.lon, -2, 0.000001);
		}

		// no decimal part, but still valid
		{
			POI poi = HRTProtocol.unmarshall("-1.,-2.");
			assertTrue(poi != null);
			assertEquals(poi.lat, -1, 0.000001);
			assertEquals(poi.lon, -2, 0.000001);
		}

		// shortest decimal part
		{
			POI poi = HRTProtocol.unmarshall("-1.0,-2.0");
			assertTrue(poi != null);
			assertEquals(poi.lat, -1, 0.000001);
			assertEquals(poi.lon, -2, 0.000001);
		}

		// longest decimal part
		{
			POI poi = HRTProtocol.unmarshall("-1.000000,-2.000000");
			assertTrue(poi != null);
			assertEquals(poi.lat, -1, 0.000001);
			assertEquals(poi.lon, -2, 0.000001);
		}

		// too long decimal part
		{
			POI poi = HRTProtocol.unmarshall("-1.0000000,-2.0000000");
			assertTrue(poi == null);
		}
	}

	public void testParsingDoublePositives() {
		// no decimal part
		{
			POI poi = HRTProtocol.unmarshall("+1.0,+2.0");
			assertTrue(poi != null);
			assertEquals(poi.lat, 1, 0.000001);
			assertEquals(poi.lon, 2, 0.000001);
		}

		// too long decimal part
		{
			POI poi = HRTProtocol.unmarshall("+1.0000000,+2.0000000");
			assertTrue(poi == null);
		}
	}
}
