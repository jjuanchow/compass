package com.sgr_b2.compass.tests;

import junit.framework.TestCase;
import android.location.Location;

import com.sgr_b2.compass.navigation.MonitorEntry;
import com.sgr_b2.compass.navigation.Router;
import com.sgr_b2.compass.navigation.Tracker;


public class RouterTest extends TestCase {

	Router router = null;

	protected void setUp() throws Exception {
		super.setUp();

		this.router = new Router();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testLocationAzimuthTTL() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setBearing(11.1f);

		// pass tracker's threshold
		for (int i = 0; i < Tracker.LOCATION_READINGS_THRESHOLD + 1; ++i) {
			router.updateLocation(location, System.currentTimeMillis());
		}
		router.updateMagneticHeading(12.2f);

		float lowpass_factor = (1 - Tracker.ANGLE_ALPHA);

		MonitorEntry entry = new MonitorEntry();
		router.selectHeading(entry, System.currentTimeMillis());
		assertFalse(entry.direction_is_magnetic);
		assertEquals((int)(11 * lowpass_factor), entry.azimuth, 0.1f);

		try {
			Thread.sleep(Router.LOCATION_TTL / 2, 0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// check heading is still there
		router.selectHeading(entry, System.currentTimeMillis());
		assertFalse(entry.direction_is_magnetic);
		assertEquals((int)(11 * lowpass_factor), entry.azimuth, 0.1f);

		try {
			Thread.sleep(Router.LOCATION_TTL / 2 + 1000, 0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		router.selectHeading(entry, System.currentTimeMillis());
		assertTrue(entry.direction_is_magnetic);
		assertEquals((int)(12 * lowpass_factor), entry.azimuth, 0.1f);
	}

	public void testSpeedUpdates() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setSpeed(1.1f);

		router.updateLocation(location, System.currentTimeMillis());

		float lowpass_factor = (1 - Tracker.SPEED_ALPHA);

		MonitorEntry entry = new MonitorEntry();
		router.selectSpeed(entry, System.currentTimeMillis());
		assertEquals(1.1f * lowpass_factor, entry.speed, 0.1f);

		try {
			Thread.sleep(Router.SPEED_TTL / 2, 0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// check speed is still there
		router.selectSpeed(entry, System.currentTimeMillis());
		assertEquals(1.1f * lowpass_factor, entry.speed, 0.1f);

		try {
			Thread.sleep(Router.SPEED_TTL / 2 + 1000, 0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		router.selectSpeed(entry, System.currentTimeMillis());
		assertEquals(0, entry.speed, 0.1f);
	}

	public void testWeakMagneticDetection() {
		{
			MonitorEntry entry = new MonitorEntry();

			router.updateMagneticHeading(12.2f);
			router.selectHeading(entry, System.currentTimeMillis());
			assertTrue(entry.direction_is_magnetic);
			assertTrue(entry.azimuth > 0);
		}

		{
			MonitorEntry entry = new MonitorEntry();

			router.setExtra(Router.EXTRA_MAG_IN_RANGE, 0);
			router.selectHeading(entry, System.currentTimeMillis());

			// should get magnetic direction even if sensor fails
			assertTrue(entry.direction_is_magnetic);
			assertTrue(entry.azimuth > 0);
		}
	}
}
