package com.sgr_b2.compass.tests;

import java.text.ParseException;

import junit.framework.TestCase;

import com.sgr_b2.compass.ui.UnitsFormatting;

public class UnitsFormattingTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCoordsFormatting() {
		assertEquals("0.000000", UnitsFormatting.formatCoord(0));
		assertEquals("1.234568", UnitsFormatting.formatCoord(1.23456789));
		assertEquals("11.234568", UnitsFormatting.formatCoord(11.23456789));
		assertEquals("-11.234568", UnitsFormatting.formatCoord(-11.23456789));
		assertEquals("-1.234568", UnitsFormatting.formatCoord(-1.23456789));
	}

	public void testCoordsParsing() throws ParseException {
		assertEquals(0, UnitsFormatting.parseCoord("0.000000"), 0.000001);
		assertEquals(1.234567, UnitsFormatting.parseCoord("1.234567"), 0.000001);
		assertEquals(11.234567, UnitsFormatting.parseCoord("11.234567"), 0.000001);
		assertEquals(-11.234567, UnitsFormatting.parseCoord("-11.234567"), 0.000001);
		assertEquals(-1.234567, UnitsFormatting.parseCoord("-1.234567"), 0.000001);

		assertEquals(0, UnitsFormatting.parseCoord("0,000000"), 0.000001);
		assertEquals(1.234567, UnitsFormatting.parseCoord("1,234567"), 0.000001);
		assertEquals(11.234567, UnitsFormatting.parseCoord("11,234567"), 0.000001);
		assertEquals(-11.234567, UnitsFormatting.parseCoord("-11,234567"), 0.000001);
		assertEquals(-1.234567, UnitsFormatting.parseCoord("-1,234567"), 0.000001);
	}

	public void testDistanceFormatting() {
		// TODO
	}

	public void testLocationFormatting() {
		// TODO
	}
}
