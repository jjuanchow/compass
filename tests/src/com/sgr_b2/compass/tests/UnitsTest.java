package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.navigation.Units;

public class UnitsTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testUnitsConversion() {
		// km/h to m/s
		assertEquals(0f, Units.kmph2mps(0), 0.01f);
		assertEquals(16.66f, Units.kmph2mps(60), 0.01f);
		assertEquals(-16.66f, Units.kmph2mps(-60), 0.01f);

		// m/s to km/h
		assertEquals(0f, Units.mps2kmph(0), 0.01f);
		assertEquals(60f, Units.mps2kmph(16.666f), 0.01f);
		assertEquals(-60f, Units.mps2kmph(-16.666f), 0.01f);

		// m to feet
		assertEquals(0f, Units.m2ft(0), 0.01f);
		assertEquals(36.0892f, Units.m2ft(11), 0.0001f);
		assertEquals(-36.0892f, Units.m2ft(-11), 0.0001f);

		// km to miles
		assertEquals(0f, Units.km2mi(0), 0.01f);
		assertEquals(6.83508f, Units.km2mi(11), 0.00001f);
		assertEquals(-6.83508f, Units.km2mi(-11), 0.00001f);
	}
}
