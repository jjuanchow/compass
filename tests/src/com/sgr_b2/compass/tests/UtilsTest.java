package com.sgr_b2.compass.tests;

import com.sgr_b2.compass.navigation.Utils;

import android.location.Location;

import junit.framework.TestCase;

public class UtilsTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDirection() {
		Location here = new Location("Me");
		here.setLatitude(0);
		here.setLongitude(0);

		Location there = new Location("Me");
		there.setLatitude(0);
		there.setLongitude(1);

		assertEquals(90f, Utils.direction(here, there), 0.01f);

		there.setLatitude(1);
		assertEquals(45f, Utils.direction(here, there), 0.01f);

		there.setLatitude(-1);
		there.setLongitude(-1);
		assertEquals(-135f, Utils.direction(here, there), 0.01f);

		there.setLatitude(0);
		there.setLongitude(-1);
		assertEquals(-90f, Utils.direction(here, there), 0.01f);
	}

	public void testAzimuthNormalization() {
		assertEquals(0, Utils.normalizeAzimuth(0), 0.01);
		assertEquals(359, Utils.normalizeAzimuth(359), 0.01);
		assertEquals(0, Utils.normalizeAzimuth(360), 0.01);
		assertEquals(1, Utils.normalizeAzimuth(361), 0.01);

		assertEquals(359, Utils.normalizeAzimuth(-1), 0.01);
		assertEquals(359, Utils.normalizeAzimuth(-361), 0.01);
		assertEquals(0, Utils.normalizeAzimuth(-0), 0.01);
	}

	public void testSanitization() {
		assertEquals(0, Utils.sanitizeLat(0), 0.000001);
		assertEquals(0, Utils.sanitizeLon(0), 0.000001);

		assertEquals(-90, Utils.sanitizeLat(-90), 0.000001);
		assertEquals(90, Utils.sanitizeLat(90), 0.000001);
		assertEquals(-180, Utils.sanitizeLon(-180), 0.000001);
		assertEquals(180, Utils.sanitizeLon(180), 0.000001);

		assertEquals(-90, Utils.sanitizeLat(-91.5), 0.000001);
		assertEquals(90, Utils.sanitizeLat(91.5), 0.000001);
		assertEquals(-180, Utils.sanitizeLon(-181.5), 0.000001);
		assertEquals(180, Utils.sanitizeLon(181.5), 0.000001);
	}

	public void testSpeed() {
		assertEquals(0, Utils.speed(0, 1000), 0.01f);
		assertEquals(1, Utils.speed(1, 1000), 0.01f);
		assertEquals(10, Utils.speed(100, 10 * 1000), 0.01f);
		assertEquals(-10, Utils.speed(-100, 10 * 1000), 0.01f);
		assertEquals(-1, Utils.speed(-1, 1000), 0.01f);
	}

	public void testMagneticFieldStrength() {
		float[] values = new float[3];
		values[0] = 1; values[1] = 2; values[2] = 3;

		assertEquals(3.7416, Utils.magneticFieldStrength(values), 0.0001);
	}

	public void testMagneticFieldRange() {
		assertFalse(Utils.magneticFieldValid(0));
		assertFalse(Utils.magneticFieldValid(1000));
		assertFalse(Utils.magneticFieldValid(80.1));
		assertFalse(Utils.magneticFieldValid(19.999));
		assertTrue(Utils.magneticFieldValid(20));
		assertTrue(Utils.magneticFieldValid(70));
		assertTrue(Utils.magneticFieldValid(21));
		assertTrue(Utils.magneticFieldValid(69));
		assertTrue(Utils.magneticFieldValid(45));
		assertTrue(Utils.magneticFieldValid(45.5));
	}
}
